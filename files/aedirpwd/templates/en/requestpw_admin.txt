Dear zone admin,

a password reset for a user entry was requested.

Your actions if this request seems ok:
1. Please contact the user by telephone or personally.
2. Tell the user his reset password you can read in his entry.

If someone falsely invoked the password request you can simply
ignore this message.

User name:
{username}

Distinguished name of user entry:
{userdn}

IP address from which password reset was invoked:
{remote_ip}

Show user entry with reset password:
https://{web_ctx_host}/web2ldap?{ldap_uri}/{userdn}??base??bindname=
