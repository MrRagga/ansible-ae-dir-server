This directory contains an ansible role for installing/configuring Æ-DIR servers.

Please see the [Æ-DIR installation docs](https://www.ae-dir.com/install.html)
for detailed instructions how to use this role.
