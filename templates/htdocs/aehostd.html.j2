{% extends aedir_htdocs_layout %}

{% block head_title %}aehostd -- Custom NSS/PAM demon{% endblock %}
{% block head_pagedescription %}aehostd -- Custom NSS/PAM demon{% endblock %}

{% block content %}

<p>
  <strong>Intended Audience:</strong>
  System architects, developers and system administrators
</p>

<ol>
  <li>
    <a href="#intro">Introduction</a></li>
    <ol>
      <li><a href="#features">Specific features</a></li>
      <li><a href="#maps">NSS maps</a></li>
      <li><a href="#aehost-init">Host password initialization</a></li>
    </ol>
  </li>
  <li>
    <a href="#install">Installation</a>
    <ol>
      <li><a href="#pam-nss-modules">NSS/PAM modules</a></li>
    </ol>
  </li>
  <li>
    <a href="#cfg">Configuration</a>
    <ol>
      <li><a href="#cfg-general">General</a></li>
      <li><a href="#cfg-socket">Socket</a></li>
      <li><a href="#cfg-ldap">LDAP</a></li>
      <li><a href="#cfg-maps">NSS maps</a></li>
      <li><a href="#cfg-pam">PAM</a></li>
    </ol>
  </li>
</ol>

<h1 id="intro">Introduction</h1>

<p>
  While you can integrate your Linux systems with any NSS/PAM service demon
  it is highly recommended to use
  <a href="https://pypi.org/project/aehostd/">aehostd</a>,
  a custom NSS/PAM service implemented
  in <a href="https://www.python.org">Python 2.x</a>.
</p>

<p>
  The following diagram illustrates the integration of <em>aehostd</em> into
  Linux login:
</p>

{% set img_anchor="aehostd" -%}
{% set img_file="aehostd.png" -%}
{% set img_title="&AElig;-DIR integration of Linux with aehostd" -%}
{% include "includes/img.html.j2" %}

<h2 id="features">Specific features</h2>
<p>
  Additionally to what you expect by such a service this custom client
  is has specific functionality for use with &AElig;-DIR:
</p>
<ul>
  <li>
    Binds to &AElig;-DIR either with
    <ul>
      <li><em>simple bind</em> (short-DN form)</li>
      <li><em>SASL/EXTERNAL</em> bind when using TLS client certs</li>
    </ul>
  </li>
  <li>
    Sends <em>LDAP Who Am I?</em> extended operation
    (see <a href="https://tools.ietf.org/html/rfc4532">RFC 4352</a>)
    to find the real DN of their own
    <a href="docs.html#schema-oc-aeHost">aeHost</a> entry
    for determining <a href="docs.html#schema-oc-aeSrvGroup">aeSrvGroup</a>
    membership.
  </li>
  <li>
    Without specific client configuration it uses search filters for
    searching user, group and sudoers entries based on the attributes found
    in all relevant
    <a href="docs.html#schema-oc-aeSrvGroup">aeSrvGroup</a>
    entries searched before:
    <ul>
      <li>aeLoginGroups</li>
      <li>aeVisibleGroups</li>
      <li>aeVisibleSudoers</li>
    </ul>
    This gives much better performance when having many POSIX systems
    integrated with &AElig;-DIR.
  </li>
  <li>
    When checking user's password it sends
    <em>Session Tracking</em> extended control along with the simple bind
    request with the IP address of the client system and the user name
    (see <a href="https://tools.ietf.org/html/draft-wahl-ldap-session">draft-wahl-ldap-session</a>).
    This gives more valuable information in &AElig;-DIR slapd logs for auditing.
  </li>
  <li>
    Synchronization of SSH authorized keys to a configurable directory.
  </li>
  <li>
    Synchronization of sudoers content to a configurable file.
  </li>
</ul>

<h2 id="maps">NSS maps</h2>
<p>
  The following NSS maps are provided:
</p>
<ul>
  <li>passwd</li>
  <li>group</li>
  <li>hosts</li>
</ul>

<p>
  Besides normal <em>group</em> map this demon returns some virtual
  groups to the calling application:
  <ul>
    <li>
      Virtual groups for the individual <var>gidNumber</var> set in
      <a href="docs.html#schema-oc-aeUser-attributes">aeUser</a> entries.
    </li>
    <li>
      Virtual role groups based on
      <a href="docs.html#schema-oc-aeSrvGroup-attributes">
      rights groups attributes</a>
      <table>
        <tr>
          <th>Attribute</th>
          <th>Group name</th>
          <th>GID</th>
        </tr>
        <tr>
          <td>aeVisibleGroups</td>
          <td>ae-vgrp-role-all</td>
          <td>9000</td>
        </tr>
        <tr>
          <td>aeLoginGroups</td>
          <td>ae-vgrp-role-log</td>
          <td>9001</td>
        </tr>
        <tr>
          <td>aeLogStoreGroups</td>
          <td>ae-vgrp-role-login</td>
          <td>9002</td>
        </tr>
        <tr>
          <td>aeSetupGroups</td>
          <td>ae-vgrp-role-setup</td>
          <td>9003</td>
        </tr>
      </table>
    </li>
  </ul>
</p>

<p>
  Example use-cases for virtual role groups:
</p>
<ul>
  <li>
    Allow all <a href="docs.html#role-setup-admin">setup admins</a> mapped
    to virtual role POSIX group <var>ae-vgrp-role-setup</var> to use
    command <code>su</code> with own password
    (see <a href="https://linux.die.net/man/8/pam_wheel">pam_wheel(8)</a>).
  </li>
  <li>
    Allow all users with log view right mapped to virtual role POSIX group
    <var>ae-vgrp-role-log</var> to read log files by simply setting normal
    group ownership and group permissions (no need to sync POSIX ACLs).
  </li>
</ul>

<h2 id="aehost-init">Initialization of host password</h2>
<p>
  <em>aehostd</em> has a special feature which is very helpful for
  automated enrollment of hosts. It does not require administrative access
  to the machine before correct initialization and also does not require
  other agents besides <em>aehostd</em> to be installed on the host. Mainly
  it is based on a PAM authentication with host password.
</p>

{% set img_anchor="aehost-init" -%}
{% set img_file="aehost-init.svg" -%}
{% set img_title="Initializing host password in aehostd" -%}
{% include "includes/img.html.j2" %}

<p>Prerequisites:</p>
<p>
  Host gets installed with correctly configured short bind-DN
  based on the canonical hostname (FQDN)
  (usually in file <em>/etc/aehostd.conf</em>)
  but without a host password
  (usually in file <em>/var/lib/aehostd/aehostd.pw</em>).
</p>

<p>Process steps:</p>
<ol>
  <li>
    A responsible
    <a href="docs.html#role-setup-admin">setup admin</a> adds a new
    <a href="docs.html#schema-oc-aeHost">aeHost</a> entry for the canonical
    hostname (FQDN) and sets a new random password for this entry<br>
    or just sets a new random password for an existing
    <a href="docs.html#schema-oc-aeHost">aeHost</a> entry.
  </li>
  <li>
    <a href="docs.html#role-setup-admin">Setup admin</a> connects via SSH
    to the host authenticating as special service account
    <var>aehost-init</var> with the new host password set before.
  </li>
  <li>
    <em>pam_aedir</em> receives the PAM authentication request from <em>sshd</em>.
  </li>
  <li>
    <em>aehostd</em> receives the PAM authentication request for the system
    user account <var>aehost-init</var>.
  </li>
  <li>
    The host password is validated by sending a simple bind
    request on behalf of the locally configured host bind DN.
  </li>
  <li>
    In case the host password is correct it is written to the
    <em>aehostd</em> password file (located by configuration option
    <var>bindpwfile</var>) in case the password stored therein is
    different.
  </li>
</ol>

<h1 id="install">Installation</h1>

<h2 id="pam-nss-modules">NSS/PAM modules</h2>
<p>
  The NSS and PAM front-end modules of Arthur de Jong's <a
  href="https://arthurdejong.org/nss-pam-ldapd/">nss-pam-ldapd</a> (aka
  nslcd) are used for querying the <em>aehostd</em> service via its Unix
  domain socket.
</p>
<p>
  You can compile these modules with different compile-time parameters
  to prevent naming and package collisions with other standard OS packages.
  In the following example the modules are compiled with module name
  <em>aedir</em> and for using Unix domain socket
  <em>/var/run/aehostd/aehostd.sock</em>:
</p>
<pre class="cli">
./configure \
  --with-module-name=aedir \
  --disable-nslcd --disable-pynslcd --disable-kerberos \
  --libdir=/lib64 \
  --with-pam-seclib-dir=/lib/security \
  --disable-utils \
  --with-nss-maps=passwd,group,hosts \
  --with-nslcd-socket=/var/run/aehostd/aehostd.sock
make
make install
</pre>
<p>
  In <em>/etc/nsswitch.conf</em> you add the following lines:
</p>
<pre class="cli">
passwd: files aedir
group:  files aedir
</pre>

<p>OS-specific build files:</p>
<ul>
  <li>
    For building Debian packages you can use files in directory<br>
    <em>client-examples/aehostd-modules/debian/</em>
  </li>
  <li>
    For building RedHat, CentOS, Fedora packages you can use file<br>
    <em>client-examples/aehostd-modules/redhat-rpmbuild/SPECS/aehostd-modules.spec</em>
  </li>
  <li>
    For <a href="https://www.opensuse.org">openSUSE</a> and
    <a href="https://www.suse.com/products/server/">SLES</a>
    there are ready-to-use packages available at
    <a href="https://build.opensuse.org/package/show/home:stroeder:AE-DIR/aehostd-modules">
      openSUSE build service: Package <em>aehostd-modules</em>
    </a>
  </li>
</ul>

<h2 id="config">ansible</h2>
<p>
  Example ansible role: <code>ansible/roles/ae-dir-hostd/</code>
  (sets ansible variable <em>nsswitch_module</em> to "aedir")
</p>

<h1 id="options">Configuration</h1>
<p>
  The following options can be set in the configuration file <em>/etc/aehostd.conf</em>.
</p>

<h2 id="cfg-general">General</h2>
<p>
  The following options specify general process parameters:
</p>
<pre class="cfg">
# the user id nslcd should be run as
uid = aehostd
# the group id nslcd should be run as
gid = aehostd
# logging level
loglevel = 20
</pre>

<h2 id="cfg-socket">Socket</h2>
<p>
  The following options specify handling of the Unix domain socket on which
  <em>aehostd</em> listens for NSS and PAM requests:
</p>
<pre class="cfg">
# full path name of service socket
socketpath = /var/run/nslcd/socket
# timeout of service socket
sockettimeout = 10.0
# permissions used for service socket
socketperms = '0666'
</pre>

<h2 id="cfg-ldap">LDAP</h2>
<p>
  Various LDAP connection parameters can be set and tuned:
</p>
<pre class="cfg">
# The location at which the LDAP server(s) should be reachable.
uri = ldaps://ae-dir-c1.example.com ldaps://ae-dir-c2.example.com ldaps://ae-dir-c3.example.com
# preferred short bind-DN form (relocatable)
binddn = host=host1.example.com,ou=ae-dir
# pathname of file containing password for the above binddn
bindpwfile = /etc/aehostd.pw
# CA certificate for checking TLS server cert of LDAP server(s)
tls_cacertfile = /etc/ssl/certs/cacerts.pem
# Client certificate and its private key to use with SASL/EXTERNAL bind
tls_cert = None
tls_key = None
# time limit (secs) for all LDAP operations
timelimit = 3.0
# how long to use an existing LDAP connection (secs)
conn_ttl = 1800.0
# LDAPObject cache TTL (secs)
cache_ttl = 6.0
</pre>

<h2 id="cfg-maps">NSS maps</h2>
<p>
  Parameters related to NSS maps:
</p>
<pre class="cfg">
# passwd entries to ignore (default: all users in local /etc/passwd)
nss_ignore_users =
nss_ignore_uids =
# group entries to ignore (default: all users in local /etc/passwd)
nss_ignore_groups =
nss_ignore_gids =
# POSIX-ID and name validation
nss_min_uid = 0
nss_min_gid = 0
nss_max_uid = 65500
nss_max_gid = 65500
# regex for constraining valid user and group names (default: POSIX standard)
validnames =
# virtual groups
vgroup_name_prefix = ae-vgrp-
vgroup_rgid_offset = 9000
# user account used to authenticate as own aeHost object
# formatted like entry in /etc/passwd
aehost_vaccount = aehost-init:x:9042:9042:AE-DIR virtual host init account:/tmp:/bin/true
# Template string for deriving gecos field from user's name
gecos_tmpl = AE-DIR user {username}
# Template string for deriving homeDirectory value from user's name
homedir_tmpl = /home/{username}
</pre>

<h2 id="cfg-pam">PAM</h2>
<p>Parameters related to PAM:</p>
<pre class="cfg">
# filter template string to be used when processing PAM authorization requests
pam_authz_search = (&(uid={username})(|(pwdChangedTime=*)(userCertificate=*)(sshPublicKey=*)))
# Text to output when rejecting change password requests
pam_passmod_deny_msg = None
</pre>

{% endblock content %}
