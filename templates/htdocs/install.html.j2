{% extends aedir_htdocs_layout %}

{% block head_title %}Installation{% endblock %}
{% block head_pagedescription %}Installation{% endblock %}

{% block content %}
<p>
  <strong>Intended Audience:</strong>
  System administrators
</p>

<ol>
  <li><a href="#support">Support</a></li>
  <li><a href="#prereq">Prerequisites</a></li>
  <li><a href="#install">Installation</a></li>
  <li><a href="#license">Copyright &amp; License</a></li>
  <li><a href="#software">Software / technology used</a></li>
</ol>

<h1 id="support">Support</h1>
<ul>
  <li>
    <a href="mailto:michael@stroeder.com?SUBJECT=Subscribe to ae-dir-users">Subscribe to ae-dir-users</a>
    and get informed about news.
  </li>
  <li>
    If you need more help please consider
    <a href="https://www.stroeder.com/impressum.html">commercial support</a>.
  </li>
</ul>

<h1 id="prereq">Prerequisites</h1>
<ol>
  <li>
    Make yourself familiar with the
    <a href="docs.html#sys-arch">system architecture</a>.
  </li>
  <li>
    Install &AElig;-DIR servers with one of the supported operating
    systems. Currently the ansible playbooks support fully automated
    installation/configuration on&hellip;
    <ul>
      <li>
        <a href="https://en.opensuse.org/openSUSE:Tumbleweed_installation">
        openSUSE Tumbleweed</a>
      </li>
      <li>
        <a href="https://software.opensuse.org/distributions/leap">
        openSUSE Leap 15.0+</a>
      </li>
      <li>
        <a href="https://www.suse.com/products/server/">
        SUSE Linux Enterprise Server 15</a> (to be tested)
      </li>
      <li>
        <a href="https://www.debian.org/releases/stretch/">Debian Stretch</a>
        using OpenLDAP packages from
        <a href="https://ltb-project.org/wiki/documentation/openldap-deb#apt_repository">
          Debian APT repository of LTB project
        </a>
      </li>
      <li>
        <a href="https://www.centos.org/">CentOS 7.4+</a>
        using OpenLDAP packages from
        <a href="https://ltb-project.org/wiki/documentation/openldap-rpm#yum_repository">
          YUM repository of LTB project
        </a>
      </li>
    </ul>
    If you want to tweak the ansible roles to install on another OS make
    sure a recent <a href="https://www.openldap.org">OpenLDAP 2.4.44+</a>
    with overlay <em>slapo-deref</em> is available for your OS platform.
    Older releases are explicitly not recommended!
  </li>
  <li>
    The ansible roles needs the following software packages on all target
    machines to be installed:
    <ul>
      <li>Python 2.x</li>
      <li>python-xml</li>
      <li>lsb_release command (package <em>lsb-release</em>)</li>
    </ul>
  </li>
  <li>
    Install on your admin workstation (the <em>ansible controller</em>):
    <ul>
      <li>
        <a href="https://docs.ansible.com/ansible/intro_installation.html">ansible 2.4+</a>
        (Use Jinja2 2.8.x! <em>ansible</em> has some issues with 2.9+!,
        see <a href="https://github.com/ansible/ansible/issues/20063">issue 20063</a>)
      </li>
      <li>
        <a href="https://git-scm.com/book/en/v2/Getting-Started-Installing-Git">git</a>
      </li>
      <li>
        <a href="http://www.dnspython.org">dnspython</a>
      </li>
    </ul>
    Simple approach:
    <pre>
      # virtualenv-2.7 /opt/ansible
      # /opt/ansible/bin/pip2 install --upgrade ansible Jinja2==2.8.1 dnspython paramiko
    </pre>
  </li>
  <li>
    Create DNS entries for all your &AElig;-DIR servers
    following <a href="bcp.html#ae-dir-hostnames">best practices for hostnames</a>.
  </li>
  <li>
    Configure time synchronisation (NTP) required for reliable replication.
  </li>
  <li>
    Prepare to have SSH access to all &AElig;-DIR servers as user
    <em>root</em> (via <code>su</code> or <code>sudo</code>)
  </li>
  <li>
    Make yourself familiar with how to use command-line options for
    <a href="https://docs.ansible.com/ansible/become.html#command-line-options">ansible become</a>.
  </li>
  <li>
    Check whether you can access the hosts with
    <a href="https://docs.ansible.com/ansible/setup_module.html">ansible setup</a>:
    <pre>/opt/ansible/bin/ansible all -i 'hostname.example.com,' -m setup</pre>
    The trailing comma after the FQDN is needed when using a hostname!
  </li>
  <li>
    Get the ansible playbooks and roles:
    <pre>git clone --recurse-submodules https://gitlab.com/ae-dir/ansible-example-site myenv</pre>
  </li>
  <li>
    Issue X.509 TLS server certificates with appropriate <var>CN</var> and
    <var>subjectAltName</var> values for all replicas with your existing
    PKI's certificate authority.
    <br>
    The anti-security concept of wild-card certificates is not compatible
    with &AElig;-DIR's security concept! Therefore these cannot be used!
    <br>
    If you don't have a PKI yet you can setup a test certificate authority
    (CA) with shell scripts found in <code>tools/pki-scripts/</code>.
  </li>
</ol>

<h1 id="install">Installation</h1>
<ol>
  <li>
    Edit ansible inventory file <em>myenv/hosts</em> to match your
    hosts/VMs/containers of your installation environment.
  </li>
  <li>
    Read comments in file
    <code>myenv/roles/ae-dir-server/defaults/main.yml</code> and adjust
    <a href="https://docs.ansible.com/ansible/playbooks_variables.html">
    ansible group and host vars</a> to match your environment.
  </li>
  <li>
    Invoke ansible play in sub-directory <code>ansible/</code>
    (here using command <code>su</code>):
    <pre>/opt/ansible/bin/ansible-playbook ae-dir-servers.yml -i myenv/hosts --become -K --become-method=su --extra-vars='{"aedir_init":True, "openldap_keygen":True}'</pre>
    <ul>
      <li>
        At first run this will generate TLS server key and signed CSR file
        and stops with a message where to find the CSR files on your local
        ansible controller.
      </li>
      <li>
        After signing the CSRs with your CA place the server certificate file(s) into
        directory <code>ae-dir/ansible/myenv/files/</code>.
      </li>
      <li>
        Invoke <var>ansible-playbook</var> command above again to proceed
        with installation.
      </li>
    </ul>
  </li>
  <li>
    Log into one provider system become user <em>root</em> and run the
    following commands to fully initialize your directory:
    <ol>
      <li>
        Add the basic &AElig;-DIR entries with OpenLDAP command-line tool:
        <ul>
          <li>
            On SUSE / openSUSE:
            <pre>ldapmodify -f /opt/ae-dir/etc/ae-dir-base.ldif</pre>
          </li>
          <li>
            On Debian and CentOS:
            <pre>/usr/local/openldap/bin/ldapmodify -f /opt/ae-dir/etc/ae-dir-base.ldif</pre>
          </li>
        </ul>
      </li>
      <li>
        Set the user password of an initial Æ admin (here <em>msin</em>):
        <pre>/opt/ae-dir/bin/ae-passwd.py msin</pre>
      </li>
    </ol>
  </li>
  <li>
    Check the systems' health by invoking as <em>root</em> the monitoring
    script on all &AElig;-DIR servers. By default it is installed to:
    <pre>/opt/ae-dir/sbin/slapd_checkmk.sh</pre>
</ol>

<h2 id="linux-login">NSS/PAM self-integration</h2>
<p>
  For PAM/NSS client self-integration invoke ansible play in sub-directory <code>ansible/</code>
  (here using command <code>su</code>):
</p>
<pre class="cli">
/opt/ansible/bin/ansible-playbook ae-dir-clients.yml -i myenv/hosts --become -K --become-method=su -l ae-dir-servers
</pre>

<h2 id="oath-ldap">Two-factor authentication (OATH-LDAP)</h2>
<p>
  You can easily use the built-in two-factor authentication based on
  <a href="https://oath-ldap.stroeder.com/">OATH-LDAP</a>.
</p>
<p>
  This is enabled by setting ansible variable
  <var>oath_ldap_enabled: True</var> and then play the complete
  configuration to the &AElig;-DIR servers. Of course you set this flag
  also before the first run. This installs an additional web app and the
  so-called bind listeners on providers and consumers.
</p>
<p>
  Afterwards you have to generate at least one master key pair for
  protecting the token shared secrets (OTP seeds):
</p>
<pre class="cli">
# oathldap-tool genkey --key-path /opt/ae-dir/etc/oath-master-keys/
Generating RSA-2048 key pair...

wrote /opt/ae-dir/etc/oath-master-keys/oathldap_master_key_201806141818.priv
wrote /opt/ae-dir/etc/oath-master-keys/oathldap_master_key_201806141818.pub
</pre>
<p>
  Correct the permissions if needed which would also be done by next ansible play:
</p>
<pre class="cli">
# chmod 640 /opt/ae-dir/etc/oath-master-keys/oathldap_master_key_201806141818.*
</pre>
<p>
  Then store the new public key in the OATH parameters entry:
</p>
<pre class="cli">
# ldapmodify &lt;&lt;EOF
dn: cn=oath-policy-hotp-users,cn=ae,ou=ae-dir
changetype: modify
replace: oathEncKey
oathEncKey:&lt; file:/opt/ae-dir/etc/oath-master-keys/oathldap_master_key_201806141818.pub

EOF
</pre>

<h1 id="license">Copyright &amp; License</h1>
<p>
  &copy; 2015-2017 by <a href="https://stroeder.com/impressum.html">Michael Str&ouml;der</a>
</p>
<pre>
  Licensed under the Apache License, Version 2.0 (the "License"); you may
  not use files and content provided on this web site except in compliance
  with the License. You may obtain a copy of the License at

      <a href="http://www.apache.org/licenses/LICENSE-2.0">http://www.apache.org/licenses/LICENSE-2.0</a>

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
</pre>

<h1 id="software">Software / technology used</h1>
<p>
  &AElig;-DIR serves as a good example for
  <a href="https://en.wikipedia.org/wiki/Standing_on_the_shoulders_of_giants">
  standing on the shoulders of giants</a>:
</p>
<ul>
  <li><a href="https://www.kernel.org/">Linux</a></li>
  <li><a href="https://www.openldap.org">OpenLDAP 2.4.x server and client libs</a></li>
  <li><a href="https://www.python.org">Python 2.x</a></li>
  <li><a href="https://git-scm.com/">git</a></li>
  <li><a href="https://www.ansible.com">ansible</a></li>
  <li><a href="https://httpd.apache.org">Apache 2.4.x</a></li>
  <li><a href="https://web2ldap.de">web2ldap</a></li>
  <li><a href="https://pypi.python.org/pypi/ldap0">ldap0</a></li>
  <li><a href="https://www.stroeder.com/slapdsock.html">slapdsock</a></li>
  <li><a href="https://oath-ldap.stroeder.com/">OATH-LDAP</a></li>
</ul>
{% endblock content %}
