{% extends aedir_htdocs_layout %}

{% block head_title %}Applications{% endblock %}
{% block head_pagedescription %}Application integration examples{% endblock %}

{% block content %}

<p>
  <strong>Intended Audience:</strong>
  System architects, developers and system administrators
</p>

<ol>
  <li>
    <a href="#client-params">Client parameters</a>
    <ol>
      <li><a href="#search-params">Search parameters</a></li>
      <li>
        <a href="#bind-params">Bind parameters</a>
        <ol>
          <li><a href="#tls-params">TLS parameters</a></li>
          <li><a href="#simple-bind">Simple bind</a></li>
          <li><a href="#sasl-external-bind">SASL/EXTERNAL</a></li>
        </ol>
      </li>
    </ol>
  </li>
  <li>
    <a href="#pam-nss">PAM/NSS services</a>
    <ol>
      <li>
        <a href="#pam-nss-simple">Simple PAM/NSS clients</a>
        <ol>
          <li><a href="#pam-nss-sssd">sssd</a></li>
          <li><a href="#pam-nss-nslcd">nss-pam-ldapd (aka nslcd)</a></li>
          <li><a href="#nsscache">nsscache</a></li>
        </ol>
      </li>
      <li>
        <a href="#pam-nss-smart">Smart PAM/NSS clients</a>
        <ol>
          <a href="#pam-nss-aehostd">aehostd -- Custom NSS/PAM demon</a>
        </ol>
      </li>
    </ol>
  </li>
  <li>
    <a href="#ssh-proxy">SSH proxy</a>
  </li>
  <li>
    <a href="#slapd-ldap">LDAP proxy for another LDAP server</a>
  </li>
  <li>
    <a href="#wifi">Wifi access point</a>
  </li>
</ol>

<h1 id="client-params">Client parameters</h1>
<p>
  Integrating an LDAP-enabled application/system with &AElig;-DIR does not
  require detailed configuration tweaking. In general setting less
  special parameters are better than more because &AElig;-DIR ACLs
  internally sort out most stuff.
</p>

<h2 id="search-params">Search parameters</h2>
<dl>
  <dt>
    Search base:
  </dt>
  <dd>
    <code>{{ aedir_suffix }}</code>
    (or whatever you configured in ansible default variable
    <var>aedir_suffix</var>)
  </dd>
  <dt>
    Search scope:
  </dt>
  <dd>
    <code>subtree</code>
  </dd>
  <dt>
    Various possible filters for searching for all visible users accounts:
  </dt>
  <dd>
    <ul>
      <li><code>(objectClass=account)</code></li>
      <li><code>(objectClass=inetOrgPerson)</code></li>
      <li><code>(objectClass=person)</code></li>
    </ul>
  </dd>
  <dt>
    Filter for searching a particular account...
  </dt>
  <dd>
    <dl>
      <dt>
        ...by username
      </dt>
      <dd>
        <code>(uid=&lt;username&gt;)</code>
      </dd>
      <dt>
        ...by e-mail address
      </dt>
      <dd>
        <code>(mail=&lt;e-mail address&gt;)</code>
      </dd>
    </dl>
  </dd>
  <dt>
    Additionally ensure the found user has login right on your system:
  </dt>
  <dd>
    <code>(&(uid=&lt;username&gt;)(pwdChangedTime=*))</code>
  </dd>
  <dt>
  </dt>
  <dd>
  </dd>
</dl>

<h2 id="bind-params">Bind parameters</h2>
<p>
  Bear in mind that your system always <strong>must bind</strong> to
  be granted access for searching users and groups etc.
</p>

<h3 id="tls-params">TLS parameters</h3>
<p>
  Clear-text connections are strictly disallowed. Therefore your system has
  to know the following parameters for estabilishing a validated TLS
  connection:
</p>
<ul>
  <li>CA certificate</li>
  <li>strict validation mode</li>
  <li>
    strong cipher suites matching what's configured with slapd.conf
    directive <var>TLSCipherSuite</var>
  </li>
</ul>

<h3 id="simple-bind">Simple bind</h3>
<dl>
  <dt>
    Bind-DN
  </dt>
  <dd>
    <p>
      The bind-DN in client configuration files should be the unique
      short-form instead of the complete DN. This allows moving the
      accompanying <a href="docs.html#schema-oc-aeHost">host</a> or
      <a href="docs.html#schema-oc-aeService">service</a> entry within
      the DIT to another <a href="docs.html#schema-oc-aeZone">zone</a> and/or
      beneath another <a href="docs.html#schema-oc-aeSrvGroup">service group</a>
      without having to reconfigure the client side.
    </p>
    <dl>
      <dt>Hosts (OS login):</dt>
      <dd>
        <code>host=&lt;canonical hostname&gt;,{{ aedir_suffix }}</code>
      </dd>
      <dt>Server services (application/service login):</dt>
      <dd>
        <code>uid=&lt;service name&gt;,{{ aedir_suffix }}</code>
      </dd>
    </dl>
  </dd>
  <dt>
  </dt>
  <dd>
  </dd>
</dl>

<h3 id="sasl-external-bind">SASL/EXTERNAL</h3>
<p>
  The OpenLDAP server can use authentication credentials available at the
  transport layer to authenticate the client if it connects via
  <a href="https://tools.ietf.org/html/draft-chu-ldap-ldapi">LDAP over IPC (LDAPI)</a>
  or with <em>TLS encryption</em> and sends a SASL bind operation with
  mechanism <em>EXTERNAL</em>.
</p>
<p>
  In case of TLS with client cert authentication the configuration requires
  these parameters to be set:
</p>
<ul>
  <li>
    client public-key certificate and key
  </li>
  <li>
    private client key (matching client cert)
  </li>
  <li>
    Subject DN of client cert must be written to attribute <var>seeAlso</var>
    in <a href="docs.html#schema-oc-aeService">aeService</a> entry in LDAP
    string representation.
  </li>
</ul>

<h1 id="pam-nss">PAM/NSS services</h1>

<h2 id="pam-nss-aehostd">aehostd -- Custom NSS/PAM demon</h2>
<p>
  While you can integrate your Linux systems with any NSS/PAM service demon
  it is highly recommended to use <a href="aehostd.html">aehostd</a>
  which gives better search performance and has features specifically
  useful when using &AElig;-DIR.
</p>

<h2 id="pam-nss-simple">Simple PAM/NSS clients</h2>
<p>
  Simple PAM/NSS clients do not have any special knowledge about
  &AElig;-DIR DIT and schema.
</p>

<h3 id="pam-nss-sssd">sssd</h3>
<p>
  While the <a href="https://pagure.io/SSSD/sssd/">System Security Services Daemon (SSSD)</a>
  for Linux is not a simple component the configuration of
  <em>sssd-ldap(5)</em> for &AElig;-DIR does deliberately not use any
  of its special features.
</p>
<p>
  The following diagram illustrates the integration of <em>sssd</em> into
  Linux login:
</p>

{% set img_anchor="linux-client-sssd" -%}
{% set img_file="linux-client-sssd.png" -%}
{% set img_title="&AElig;-DIR integration of Linux with sssd components" -%}
{% include "includes/img.html.j2" %}

<p>
  Two example configuration files for simple bind and SASL/EXTERNAL bind
  with TLS client certs:
  <a href="https://gitlab.com/ae-dir/client-examples/tree/master/sssd">client-examples/sssd</a>
</p>
<p>
  Example ansible role: <code>ansible/roles/ae-dir-linux-client/</code>
  (set ansible variable <em>nsswitch_module</em> to "sss")
</p>
<p>
  See also:
</p>
<ul>
  <li>
    <a href="https://jhrozek.wordpress.com/2015/03/11/anatomy-of-sssd-user-lookup/">
    Blog article: Anatomy of sssd user lookup</a>
  </li>
</ul>

<h3 id="pam-nss-nslcd">nss-pam-ldapd (aka nslcd)</h3>
<p>
  Arthur de Jong's <a href="https://arthurdejong.org/nss-pam-ldapd/">nss-pam-ldapd</a>
  is also a demon providing NSS and PAM services and runs on various POSIX (non-Linux) platforms.
</p>

<p>
  Example <em>nslcd.conf</em> in directory <a href="https://gitlab.com/ae-dir/client-examples/tree/master/nss-pam-ldapd">client-examples/nss-pam-ldapd</a>
</p>
<p>
  Example ansible role: <code>ansible/roles/ae-dir-linux-client/</code>
  (set ansible variable <em>nsswitch_module</em> to "ldap")
</p>

<p>
  See also:
</p>
<ul>
  <li>
    <a href="https://arthurdejong.org/nss-pam-ldapd/design">nss-pam-ldapd: Design documentation</a>
  </li>
  <li>
    <a href="https://arthurdejong.org/nss-pam-ldapd/docs">nss-pam-ldapd: Documentation</a>
  </li>
  <li>
    <a href="https://arthurdejong.org/nss-pam-ldapd/nslcd.conf.5">nslcd.conf(5) manual page</a>
  </li>
</ul>

<h3 id="nsscache">nsscache</h3>
<p>
  If you like syncing the NSS maps locally for off-line use you can
  look at <a href="https://github.com/google/nsscache">nsscache</a>
  and its NSS module <a href="https://github.com/google/libnss-cache">libnss-cache</a>.
</p>


<h1 id="ssh-proxy">SSH proxy</h1>
<p>
  It is possible to implement an authorizing SSH proxy which queries exactly
  the same <a href="docs.html#er-roles">&AElig;-DIR login relationship</a>
  to check whether an user is allowed to login to a specific target SSH
  host through the SSH proxy.
</p>

{% set img_anchor="ssh-proxy" -%}
{% set img_file="ssh-proxy.svg" -%}
{% set img_title="SSH proxy integrated with &AElig;-DIR" -%}
{% include "includes/img.html.j2" %}

<p>
  Notes:
</p>
<ul>
  <li>
    The <a href="docs.html#schema-oc-aeSrvGroup">aeSrvGroup</a>
    of the SSH proxy's <a href="docs.html#schema-oc-aeHost">aeHost</a>
    must have attribute <em>aeProxyFor</em> set referencing the
    <em>aeSrvGroup</em> of the target system or must have the general role
    <a href="docs.html#role-ae-login-proxy">&AElig; Login Proxy</a>.
  </li>
  <li>
    The user authentication at the SSH proxy is done via SSH authorized key
    and PAM using password + OTP.
  </li>
  <li>
    The user has to use
    <a href="https://man.openbsd.org/ssh_config#ProxyCommand">ProxyCommand</a>
    to open the proxy connection prior to the SSH connection to the target host.
  </li>
  <li>
    <a href="https://man.openbsd.org/ssh_config#ControlMaster">ControlMaster</a>
    connection should be used by the client to avoid reauthentication
    at the SSH proxy for subsequent connections to target systems.
  </li>
  <li>
    <a href="https://man.openbsd.org/sshd_config#ForceCommand">ForceCommand in sshd_config</a>
    restricts normal users to only invoke a wrapper script which sets up a
    simple TCP relay. The wrapper script is run as the AE-DIR user
    connecting to the SSH proxy.
  </li>
  <li>
    A small demon on the SSH proxy queried by the wrapper script over a
    Unix domain socket for deciding whether to actually allow the login or
    not. It queries <a href="docs.html#er-roles">&AElig;-DIR login relationship</a>
    defined for the <a href="docs.html#schema-oc-aeSrvGroup">aeSrvGroup</a>
    of the target system.
  </li>
</ul>

<p>
  See also:
</p>
<ul>
  <li>
    <a href="https://en.wikibooks.org/wiki/OpenSSH/Cookbook/Multiplexing">SSH Multiplexing</a>
  </li>
  <li>
    <a href="https://docs.ansible.com/ansible/latest/faq.html#how-do-i-configure-a-jump-host-to-access-servers-that-i-have-no-direct-access-to">
      Ansible FAQ: How do I configure a jump host to access servers that I have no direct access to?
    </a>
  </li>
</ul>

<h1 id="slapd-ldap">LDAP proxy for another LDAP server</h1>
<p>
  If you want to secure administrative access to another LDAP server you
  can use OpenLDAP's
  <a href="https://www.openldap.org/software/man.cgi?query=slapd-ldap">slapd-ldap(5)</a>
  (aka <em>back-ldap</em>) for integrated authentication and authorization.
</p>
<p>
  The following picture shows an example for a separate LDAP server serving
  DNS and DHCP data (or anything else) which gets maintained via LDAPS.
</p>

{% set img_anchor="slapd-ldap" -%}
{% set img_file="slapd-ldap.svg" -%}
{% set img_title="LDAP proxy integrated with &AElig;-DIR" -%}
{% include "includes/img.html.j2" %}

<p>Notes:</p>
<ul>
  <li>
    The admin is authenticated via LDAP simple bind pass-through.
  </li>
  <li>
    With its <a href="docs.html#schema-oc-aeService">service entry</a> the
    LDAP proxy queries users and groups from &AElig;-DIR and uses that for
    authorizing administrative access to the entries stored in the local
    database backend.
  </li>
  <li>
    It is recommended to use the TLS server certificate of
    the external LDAP server also for authentication to &AElig;-DIR to avoid
    the need for maintaining another system credential (e.g. service password).
  </li>
</ul>

<p>
  See also:
</p>
<ul>
  <li>
    Example configuration file with SASL/EXTERNAL bind with TLS client cert:
    <a href="https://gitlab.com/ae-dir/client-examples/tree/master/slapd-ldap">client-examples/slapd-ldap</a>
  </li>
  <li>
    <a href="https://www.openldap.org/software/man.cgi?query=slapd-ldap">
      OpenLDAP man-page slapd-ldap(5)
    </a>
  </li>
  <li>
    <a href="https://www.openldap.org/doc/admin24/backends.html#LDAP">
      LDAP backend in OpenLDAP Software 2.4 Administrator's Guide
    </a>
  </li>
</ul>

<h1 id="wifi">Wifi access point</h1>
<p>
  For setting up a very simple wireless access point using WPA2 enterprise
  authentication you can install <a
  href="https://w1.fi/hostapd/">hostapd</a> and <a
  href="https://freeradius.org/">FreeRADIUS</a> on one system and configure
  the latter as LDAP client as shown in the following picture.
</p>

{% set img_anchor="wifi-ap" -%}
{% set img_file="wifi-ap.svg" -%}
{% set img_title="Wifi access point integrated with &AElig;-DIR" -%}
{% include "includes/img.html.j2" %}

<p>Notes:</p>
<ul>
  <li>
    With its <a href="docs.html#schema-oc-aeService">service entry</a> the
    RADIUS service queries users and groups from &AElig;-DIR and uses that
    for authorizing access to your Wifi network(s).
  </li>
  <li>
    The client must use
    <a href="https://en.wikipedia.org/wiki/Extensible_Authentication_Protocol#EAP_Tunneled_Transport_Layer_Security_(EAP-TTLS)">
      EAP-TTLS
    </a>
    with PAP in inner tunnel
    (see also <a href="https://tools.ietf.org/html/rfc5281#section-11.2.5">RFC 5281</a>)
    because &AElig;-DIR does not give read access to attribute
    <var>userPassword</var> and the user's password is stored as salted
    hash. It is highly recommended to disable all other mechanisms in
    FreeRADIUS config to avoid e.g. Windows insisting on using
    <em>MSCHAP</em> or other challenge-response mechs.
  </li>
  <li>
    It is recommended to use the EAP-TLS server certificate of the RADIUS
    service also for authentication to &AElig;-DIR to avoid the need for
    maintaining another system credential (e.g. service password).
  </li>
  <li>
    The RADIUS shared secret for <em>hostapd</em> is a purely local matter.
    It is not used to secure a remote RADIUS access.
    So no need to manage it centrally.
  </li>
</ul>

<p>
  See also:
</p>
<ul>
  <li>
    Example configuration files:
    <a href="https://gitlab.com/ae-dir/client-examples/tree/master/freeradius">client-examples/freeradius</a>
  </li>
  <li>
    <a href="https://freeradius.org/documentation/">
      FreeRADIUS Documentation
    </a>
  </li>
</ul>

{% endblock content %}
